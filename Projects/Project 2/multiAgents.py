# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        currentScore = currentGameState.getScore()
        currentFoodCount = currentGameState.getNumFood()

        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newFoodCount = successorGameState.getNumFood()
        newGhostStates = successorGameState.getGhostStates()
        newGhostPositions = [s.configuration.pos for s in newGhostStates]
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        newScore = successorGameState.getScore()

        "*** YOUR CODE HERE ***"
        # calculate all distances to non-scared ghosts from new position
        distancesToGhosts = [manhattanDistance(newPos,ghostPos) for (i, ghostPos) in enumerate(newGhostPositions) if newScaredTimes[i] == 0]
        # calculate all distances to remaining pellets from new position
        distancesToPellets = [manhattanDistance(newPos,pelletPos) for pelletPos in newFood.asList()]

        # use if clause to avoid exceptions in case all ghosts are scared or all pellets are eaten in last move
        if distancesToGhosts and newFoodCount > 0:
            # deltaScore is either 10 or -1, deltaFoodCount is either -1 or 0
            value = (newScore - currentScore) - 5 * (newFoodCount - currentFoodCount) - 0.1 * min(distancesToPellets)
            if min(distancesToGhosts) < 1: 
                # prevent death
                value = value - 100
        else:
            value = (newScore - currentScore) - 5 * (newFoodCount - currentFoodCount)
        return value

def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents()
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        actions = gameState.getLegalActions(0)
        bestAction = actions[0] # initialize best action
        bestScore = float('-inf')
        for action in actions:
            successor = gameState.generateSuccessor(0, action)
            newScore = self.minScore(successor, 1, 1)
            if newScore > bestScore:
                bestAction = action
                bestScore = newScore
        return bestAction
    
    def minScore(self, gameState, currentDepth, agent):
        # agents >= 1 represent ghosts
        if gameState.isLose() or gameState.isWin():
            # if state is terminal
            return self.evaluationFunction(gameState)
        actions = gameState.getLegalActions(agent)
        worstScore = float('inf')
        for action in actions:
            successor = gameState.generateSuccessor(agent, action)
            if agent < gameState.getNumAgents() - 1:
                # if another ghost is the next agent to move
                newScore = self.minScore(successor, currentDepth, agent + 1)
            else:
                newScore = self.maxScore(successor,currentDepth)
            worstScore = min(worstScore, newScore)
        return worstScore

    def maxScore(self, gameState, currentDepth):
        currentDepth += 1
        if gameState.isLose() or gameState.isWin() or currentDepth > self.depth:
            # if depth of new ply exceeds the max depth, return evaluation of last min states
            return self.evaluationFunction(gameState)
        actions = gameState.getLegalActions(0)
        bestScore = float('-inf')
        for action in actions:
            successor = gameState.generateSuccessor(0, action)
            newScore = self.minScore(successor, currentDepth, 1)
            bestScore = max(bestScore, newScore)
        return bestScore

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        actions = gameState.getLegalActions(0)
        bestAction = actions[0]  # initialize best action
        bestScore = float('-inf')
        # alpha-beta ------
        alpha = float('-inf')
        beta = float('inf')
        # ------------------
        for action in actions:
            successor = gameState.generateSuccessor(0, action)
            newScore = self.minScore(successor, 1, 1, alpha, beta)
            if newScore > bestScore:
                bestAction = action
                bestScore = newScore
            # Beta pruning ------------
            if (bestScore > beta):
                return bestAction
            alpha = max(alpha, bestScore)
            # -------------------------
        return bestAction

    def minScore(self, gameState, currentDepth, agent, alpha, beta):
        # agents >= 1 represent ghosts
        if gameState.isLose() or gameState.isWin():
            # if state is terminal
            return self.evaluationFunction(gameState)
        actions = gameState.getLegalActions(agent)
        worstScore = float('inf')
        for action in actions:
            successor = gameState.generateSuccessor(agent, action)
            if agent < gameState.getNumAgents() - 1:
                # if another ghost is the next agent to move
                newScore = self.minScore(successor, currentDepth, agent + 1, alpha, beta)
            else:
                newScore = self.maxScore(successor, currentDepth, alpha, beta)
            worstScore = min(newScore, worstScore)
            # Alpha pruning ------------
            """
            If the current lowest value for the min node is lower than alpha (the maximum
            lower bound of possible solutions), it follows that the max node above this 
            node has another min child with larger value associated and would thus
            prefer it. This means we don't have to further update the value of this min 
            node (since in the end it will be <= worstScore) and we can prune its 
            remaining children.
            """
            if (worstScore < alpha):
                return worstScore
            beta = min(beta, worstScore)
            
            # -------------------------
        
        return worstScore

    def maxScore(self, gameState, currentDepth, alpha, beta):
        currentDepth += 1
        if gameState.isLose() or gameState.isWin() or currentDepth > self.depth:
            # if depth of new ply exceeds the max depth, return evaluation of last min states
            return self.evaluationFunction(gameState)
        actions = gameState.getLegalActions(0)
        bestScore = float('-inf')
        for action in actions:
            successor = gameState.generateSuccessor(0, action)
            newScore = self.minScore(successor, currentDepth, 1, alpha, beta)
            bestScore = max(bestScore, newScore)
            # Beta pruning ------------
            """
            If the current highest value for the max node is greater than beta (the minimum
            upper bound of possible solutions), it follows that the min node above this max
            node has another max child with a lower value associated and would thus prefer it.
            This means we don't have to futher update the value of this max node and can prune
            its remaining children.
            """
            if (bestScore > beta):
                return bestScore
            alpha = max(alpha, bestScore)
            # -------------------------
            
        return bestScore

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        actions = gameState.getLegalActions(0)
        bestAction = actions[0]  # initialize best action
        bestScore = float('-inf')
        for action in actions:
            successor = gameState.generateSuccessor(0, action)
            newScore = self.expScore(successor, 1, 1)
            if newScore > bestScore:
                bestAction = action
                bestScore = newScore
        return bestAction

    def expScore(self, gameState, currentDepth, agent):
        # agents >= 1 represent ghosts
        if gameState.isLose() or gameState.isWin():
            # if state is terminal
            return self.evaluationFunction(gameState)
        actions = gameState.getLegalActions(agent)
        p = 1/len(actions)
        expScore = 0
        for action in actions:
            successor = gameState.generateSuccessor(agent, action)
            if agent < gameState.getNumAgents() - 1:
                # if another ghost is the next agent to move
                expScore += p * self.expScore(successor, currentDepth, agent + 1)
            else:
                expScore += p * self.maxScore(successor, currentDepth)
        return expScore

    def maxScore(self, gameState, currentDepth):
        currentDepth += 1
        if gameState.isLose() or gameState.isWin() or currentDepth > self.depth:
            # if depth of new ply exceeds the max depth, return evaluation of last min states
            return self.evaluationFunction(gameState)
        actions = gameState.getLegalActions(0)
        bestScore = float('-inf')
        for action in actions:
            successor = gameState.generateSuccessor(0, action)
            newScore = self.expScore(successor, currentDepth, 1)
            bestScore = max(bestScore, newScore)
        return bestScore

def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: the evaluation function evaluates the current state according
    to two scenarios:
        (1) There are non-scared ghosts and remaining pellets in the current
        state: in this case, pacman aims to maximize the score and minimize
        both the food count and the manhattan distance to the closest pellet.
        The weights are adjusted accordingly so that (a) the score and food
        cound terms have values of the same order of magnitude, and (b) the
        manhattan term is not exceedingly scaled so as to discourage pacman
        from eating the closest pellet, which causes an increase in 
        min(distanceToPellets)(since the next closest pellet will be further
        away than the closest one);
        (2) All ghosts are scared (or there are no pellets left): pacman only
        need to worry about increasing the score and decreasing the food count
        i.e. eating pellets.
    """
    "*** YOUR CODE HERE ***"
    pacmanPos = currentGameState.getPacmanPosition()
    score = currentGameState.getScore()
    foodGrid = currentGameState.getFood()
    foodCount = currentGameState.getNumFood()
    ghostStates = currentGameState.getGhostStates()
    ghostPositions = [s.configuration.pos for s in ghostStates]
    scaredTimes = [ghostState.scaredTimer for ghostState in ghostStates]

     # calculate all distances to non-scared ghosts from new position
    distancesToGhosts = [manhattanDistance(pacmanPos, ghostPos) for (i, ghostPos) in enumerate(ghostPositions) if scaredTimes[i] == 0]
    # calculate all distances to remaining pellets from new position
    distancesToPellets = [manhattanDistance(pacmanPos, pelletPos) for pelletPos in foodGrid.asList()]

       # use if clause to avoid exceptions in case all ghosts are scared or all pellets are eaten in last move
    if distancesToGhosts and foodCount > 0:
        value = score - 5 * foodCount - 2 * min(distancesToPellets)
        if min(distancesToGhosts) < 1:
            # prevent death
            value = value - float('inf')
    else:
        value = score - 5 * foodCount
    return value

# Abbreviation
better = betterEvaluationFunction
