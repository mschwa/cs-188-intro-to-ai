# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util
from game import Actions

from learningAgents import ValueEstimationAgent
import collections

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        # initialize counter containing the values for the kth iteration
        self.values = util.Counter()
        # initialize values for all states
        self.states = self.mdp.getStates()
        for state in self.states:
            self.values[state] = 0
        # run value iteration
        self.runValueIteration()
        

    def runValueIteration(self):
        # Write value iteration code here
        "*** YOUR CODE HERE ***"
        """
            The batch version of value iteration (one array for values in the kth
            and another for values in the (k+1)th iteration) converges more slowly
            than the version of the algorithm that uses only one array for calculated
            values. This is due to the fact that in the latter case, new values are 
            used in subsequent computations as soon as they are learnt.
        """
        # initialize counter containing the values for the (k+1)th iteration
        updatedValues = self.values.copy()
        for i in range(self.iterations):
            # calculate state values for (k+1)th iteration
            for state in self.states:
                if self.mdp.isTerminal(state):
                    # value of all terminal states is zero
                    continue
                actions = self.mdp.getPossibleActions(state)
                qValues = []
                for action in actions:
                    qValues.append(self.computeQValueFromValues(state, action))
                updatedValues[state] = max(qValues)
            self.values = updatedValues.copy()


    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        successorsAndProbs = self.mdp.getTransitionStatesAndProbs(state, action)
        q = 0
        for successor, prob in successorsAndProbs:
            q += prob * (self.mdp.getReward(state, action, successor) + self.discount * self.values[successor])
        return q
        

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        actions = self.mdp.getPossibleActions(state)
        if self.mdp.isTerminal(state):
            return None
        qValues = util.Counter()
        for action in actions:
            qValues[action] = self.computeQValueFromValues(state, action)
        return qValues.argMax() 
            

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)

class AsynchronousValueIterationAgent(ValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        An AsynchronousValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs cyclic value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 1000):
        """
          Your cyclic value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy. Each iteration
          updates the value of only one state, which cycles through
          the states list. If the chosen state is terminal, nothing
          happens in that iteration.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state)
              mdp.isTerminal(state)
        """
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"
        for i in range(self.iterations):
            # update one state per iteration
            state = self.states[i % len(self.states)]
            if not self.mdp.isTerminal(state):
                actions = self.mdp.getPossibleActions(state)
                qValues = [self.computeQValueFromValues(state, action) for action in actions]
                self.values[state] = max(qValues)

class PrioritizedSweepingValueIterationAgent(AsynchronousValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A PrioritizedSweepingValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs prioritized sweeping value iteration
        for a given number of iterations using the supplied parameters.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100, theta = 1e-5):
        """
          Your prioritized sweeping value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy.
        """
        self.theta = theta
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        """
            If the calculated value of a state changes significantly after updating the value of one of its
            children, the stored value of that state should also be updated subsequently. However, not all 
            updates would be equally significant: the values of states predecessing those whose values have
            changed a lot are more likely to also change a lot. This is where pioritiziation by the size of
            the change comes in: it reduces the number of computations performed before convergence to precise
            value estimates. 

            In general, prioritized sweeping works backwards from any state whose value has changed. A 
            change in the estimated value of one state tipically implies that the values of many other
            states should also be changed, but the only useful one-step updates are those of actions that
            lead directly into the one state whose value has already been changed. If the values of these
            states are updated, then the values of the predecessor states may change in turn. This backward
            propagation goes on until all useful updates have been performed and the process is terminated.

            It is natural to prioritize the backups according to a measure of their urgency, and perform them
            in order of priority. This is the idea behind prioritized sweeping. A queue is maintained of every
            state whose estimated value would change nontrivially if updated, prioritized by the size of the change.
            When the top pair in the queue is updated, the effect on each of its predecessor pairs is computed. If
            the effect is greater than some small threshold for a predecessor, then that predecessor is inserted
            in the queue with the new priority (if there is a previous entry of the state in the queue, only the
            higher priority entry's should remain in the queue). In this way the effects of changes are efficiently
            propagated backward until quiescence.

            For more details: http://incompleteideas.net/book/first/ebook/node98.html
        """
        predecessors = self.getPredecessors()
        pQueue = self.buildPriorityQueue()
        for i in range(self.iterations):
            if pQueue.isEmpty(): 
                break
            state = pQueue.pop()
            if not self.mdp.isTerminal(state):
                qValues = [self.computeQValueFromValues(state, action) for action in self.mdp.getPossibleActions(state)]
                self.values[state] = max(qValues)
            # perform iterative sweeping
            for predecessor in predecessors[state]:
                qValuesPredecessor = [self.computeQValueFromValues(predecessor, action) for action in self.mdp.getPossibleActions(predecessor)]
                diff = abs(self.values[predecessor] - max(qValuesPredecessor))
                if diff > self.theta:
                    pQueue.update(predecessor, -diff)
            self.values = self.values.copy()

                
    def getPredecessors(self):
        """
            Maps each state to a set containing its predecessors.
        """
        # create a dictionary mapping each state to an empty set
        predecessors = dict.fromkeys(self.states, set())
        # populate sets from successor to predecessor
        for state in self.states:
            if not self.mdp.isTerminal(state):
                for action in self.mdp.getPossibleActions(state):
                    # associate each successor state with its predecessor
                    for successor, _ in self.mdp.getTransitionStatesAndProbs(state, action):
                        predecessors[successor].add(state)
        return predecessors

    def buildPriorityQueue(self):
        """
            Build initial state of priority queue used in prioritized sweeping
        """
        pQueue = util.PriorityQueue()
        # insert initial values into priority queue
        for state in self.states:
            if not self.mdp.isTerminal(state):
                qValues = [self.computeQValueFromValues(state, action) for action in self.mdp.getPossibleActions(state)]
                diff = abs(self.values[state] - max(qValues))
                pQueue.update(state, -diff)
        return pQueue
